package com.anasalmansuri.carlocationapp.Models

import java.io.Serializable

class PlaceMark(val address: String, val name: String, val coordinates: ArrayList<Double>, val engineType: String, val exterior: String, val fuel: Int, val interior: String, val vin: String) : Serializable