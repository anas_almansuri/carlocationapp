package com.anasalmansuri.carlocationapp.Presenters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anasalmansuri.carlocationapp.Models.Car
import com.anasalmansuri.carlocationapp.Models.PlaceMark
import com.anasalmansuri.carlocationapp.R
import com.anasalmansuri.carlocationapp.Views.MapsActivity
import kotlinx.android.synthetic.main.list_item.view.*

class MainAdapter(val cars: Car): RecyclerView.Adapter<CustomViewHolder>(){

    //Number of Cars
    override fun getItemCount(): Int {
        return cars.placemarks.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.list_item, parent,false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        //Display Cars on Recycler View
        val carObj = cars.placemarks[position]
        holder.view.carNameTextView.text = carObj.name
        holder.view.carAddressTextView.text= carObj.address
        holder.view.carLocationTextView.text= carObj.coordinates[0].toString() + "," + carObj.coordinates[1].toString()
        holder.view.engineTypeTextView.text= carObj.engineType
        holder.view.exteriorTextView.text= carObj.exterior
        holder.view.interiorTextView.text= carObj.interior
        holder.view.fuelTextView.text= carObj.fuel.toString()
        holder.view.vinTextView.text= carObj.vin
        holder.currentCar = carObj
    }
}

class CustomViewHolder(val view: View, var currentCar: PlaceMark?=null): RecyclerView.ViewHolder(view){
    companion object {
        val CAR_NAME= "carName"
        val CAR_LAT = "carLat"
        val CAR_LNG = "carLng"
    }
    init {
        view.setOnClickListener {
            val intent = Intent(view.context, MapsActivity::class.java)
            intent.putExtra(CAR_NAME, currentCar?.name)
            intent.putExtra(CAR_LAT, currentCar?.coordinates!![1])
            intent.putExtra(CAR_LNG, currentCar?.coordinates!![0])
            view.context.startActivity(intent)
        }
    }
}