package com.anasalmansuri.carlocationapp.Presenters

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.anasalmansuri.carlocationapp.Models.Car
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException

class ApiHandler(){
    private lateinit var carlistObject : Car
    private lateinit var progressDialog : ProgressDialog
    private val url = "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json"
    private val request = Request.Builder().url(url).build()
    private val client = OkHttpClient()

    //Fetching Json Data from the API end point and sending it to the recyclerview adapter
    fun fetchJSON(activity: Activity, context: Context, recyclerView: RecyclerView){
        progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call?, response: Response?) {
                val body = response?.body()?.string()
                val gson = GsonBuilder().create()
                carlistObject = gson.fromJson(body, Car::class.java)
                activity.runOnUiThread(Runnable {
                    progressDialog.dismiss()
                    recyclerView.adapter = MainAdapter(carlistObject)
                })
            }
            override fun onFailure(call: Call?, e: IOException?) {
                activity.runOnUiThread(Runnable {
                    progressDialog.dismiss()
                    Toast.makeText(context,"No Internet Connection!",Toast.LENGTH_LONG).show()
                })

            }
        })
    }

    //Send Data to Map Activity through Bundle
    fun sendDataToMap(): Car{
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call?, response: Response?) {
                val body = response?.body()?.string()
                val gson = GsonBuilder().create()
                carlistObject = gson.fromJson(body, Car::class.java)

            }
            override fun onFailure(call: Call?, e: IOException?) {
                TODO()
            }
        })
        return carlistObject
    }

}