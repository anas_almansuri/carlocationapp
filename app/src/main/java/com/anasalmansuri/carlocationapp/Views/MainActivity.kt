package com.anasalmansuri.carlocationapp.Views

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.anasalmansuri.carlocationapp.Presenters.ApiHandler
import com.anasalmansuri.carlocationapp.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var apiHandler: ApiHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainRecyclerView.layoutManager = LinearLayoutManager(this)
        apiHandler = ApiHandler()
        apiHandler.fetchJSON(this,this,mainRecyclerView)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean{
        menuInflater.inflate(R.menu.action_bar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val itemId = item?.itemId
        when(itemId){
            R.id.action_maps ->{
                if (mainRecyclerView.childCount != 0) {
                    val i = Intent(this@MainActivity, MapsActivity::class.java)
                    i.putExtra("carlist", apiHandler.sendDataToMap())
                    startActivity(i)
                }else{
                    Toast.makeText(this,"No internet connection available!", Toast.LENGTH_SHORT).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
