package com.anasalmansuri.carlocationapp.Views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.anasalmansuri.carlocationapp.Models.Car
import com.anasalmansuri.carlocationapp.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity() : AppCompatActivity(), OnMapReadyCallback {
    private var carlistObj : Car? = null
    private var carName: String? = null
    private var carLat: Double?=null
    private var carLng: Double?=null
    private lateinit var mMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        supportActionBar!!.title = "Go Back"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (intent.hasExtra("carlist")){
            carlistObj =  intent.getSerializableExtra("carlist") as Car
        }
        if (intent.hasExtra("carName")){
            carName = intent.getStringExtra("carName")
            carLat = intent.getDoubleExtra("carLat",0.0)
            carLng = intent.getDoubleExtra("carLng",0.0)
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        if(intent.hasExtra("carlist"))
        {
            var i = 0
            while (i < carlistObj?.placemarks!!.size){
                val location = LatLng(carlistObj?.placemarks!![i].coordinates[1],carlistObj?.placemarks!![i].coordinates[0])
                googleMap.addMarker(MarkerOptions().position(location).title(carlistObj?.placemarks!![i].name))
                i++
            }
            //Animate the camera to move to Hamburg
            val hamburg = LatLng(53.5511,9.9937)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(hamburg,13f))
        }

        if(intent.hasExtra("carName")){
            val thisCar = LatLng(carLat!!,carLng!!)
            mMap.addMarker(MarkerOptions().position(thisCar).title(carName)).showInfoWindow()
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(thisCar,15f))
        }
    }


}
